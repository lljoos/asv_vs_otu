---
title: "usASv workflow for the bacterial soil dataset: ASV vs OTU"
author: "Lisa Joos"
date: "23-1-2019"
output: html_document
---
_General information_
This script was used to create the usASv workflow in which the merged and filtered bacterial soil data is loaded into the DADA2 workflow to use the error model. This was done in regards of the manuscript "To be or not to be differential: metabarcoding analysis of soil and plant-related microbial communities by Amplicon Sequence Variants and Operational Taxonomical Units. 


```{r}
#LOAD PACKAGES
#load DADA2
library(dada2)
#check current version
#packageVersion("dada2")
#load phyloseq
library(phyloseq)
#packageVersion("phyloseq")
#load ggplot
library(ggplot2)
#packageVersion("ggplot2")

library(Biostrings)
```

#READING AND EXAMINING DATA
Load the merged and filtered data from the uparse_workflow_bacterial_soil_community
```{r}
#make variables with sample names and forward and reverse reads (in the same order)
# Sort ensures forward/reverse reads are in same order
fns <- sort(list.files(path, pattern="_merged_filtered.fq"))
# Extract sample names, assuming filenames have format: SAMPLENAME_merged_filtered.fq
sample.names <- gsub('.{19}$', '', basename(fns))

# Specify the full path to the fns and add some names to it
fns <- file.path(path, fns)
names(fns) <- sample.names
```

# Learn error rates
```{r}
#Estimate error rates for that sequencing run, use the first 1000000 reads only to limit computation time
err <- learnErrors(fns, nread=1e6, multithread=TRUE)
# err_extendedfrag <- inflateErr(getErrors(err_extendedfrag), 3) extra toevoegen ?
pdf("errorrates.pdf")
plotErrors(err, nominalQ=TRUE)
```
Convergence forward not after ten rounds, reverse after 7 rounds. 

```{r}
#INFERRING SEQUENCE VARIANTS
dds <- vector("list", length(sample.names))	#initate a list where the final data will be stored
names(dds) <- sample.names
for(sam in sample.names) {				#for each sample, do the commands in this loop
  cat("Processing:", sam, "\n")
  derep <- derepFastq(fns[[sam]])		#dereplication
  dds[[sam]] <- dada(derep, err=err, multithread=TRUE, BAND_SIZE=32) #inferring sequence variants, BAND_SIZE parameter should be adjusted in case of ITS2, 32 is recommended by the developers
}

```


```{r}
#CONSTRUCT SEQUENCE TABLE
seqtab <- makeSequenceTable(dds)			#make a sequence table of the merged reads with the counts per sample
dim(seqtab)

#Inspect distribution of sequence length
table(nchar(getSequences(seqtab)))

saveRDS(seqtab, "Output/Comparison_Uparse_DADA2_Bopact_Bacteria_Seqtab_2.rds") #save the sequence table as an RDS file, change the name according to the run
```

```{r chimera_removal}
#R code in case of using a sequence table from a single run
seqtab.nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)

#R code in case of using a merged sequence table from several sequencing runs
#seqtab <- removeBimeraDenovo(st.all, method="consensus", multithread=TRUE)

dim(seqtab.nochim)

# Check the amount
sum(seqtab.nochim)/sum(seqtab)
```

```{r taxonomy_assignment}
tax <- assignTaxonomy(seqtab.nochim,"/usr/share/qiime/data/DADA2_core_sets/silva_nr_v132_train_set.fa", multithread=TRUE)	 # Ok voor annelies 7/06/2018

# Write resulting sequence table to disk in RDS format
saveRDS(seqtab.nochim, "Output/Comparison_Uparse_DADA2_Bopact_Bacteria_seqtab.nochim_2.rds") # CHANGE ME to where you want sequence table saved
saveRDS(tax, "Output/Comparison_Uparse_DADA2_Bopact_Bacteria_Taxonomy_2.rds") # CHANGE ME ...

```

```{r count_table}
dim(seqtab.nochim)
dim(tax)

#transpose table
table<-as.data.frame(t(seqtab.nochim))

#remove rownames
rownames(table)<-NULL

#add extra columns containing taxa, for this, first transform the taxa object to a data frame
taxadf<-as.data.frame(tax)
table$Kingdom<-taxadf$Kingdom
table$Phylum<-taxadf$Phylum
table$Class<-taxadf$Class
table$Order<-taxadf$Order
table$Family<-taxadf$Family
table$Genus<-taxadf$Genus
table$Species<-taxadf$Species

#add extra column containing sequences
table$seq<-as.vector(colnames(seqtab.nochim))

#write table to output file
write.table(table,file="Output/Comparison_Uparse_DADA2_Bopact_Bacteria_Counttable_T2.txt",sep="\t",col.names=TRUE,row.names=FALSE,quote=FALSE)	#change the file name as you want it

```