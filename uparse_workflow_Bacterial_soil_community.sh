#!/bin/bash
# (c) Annelies Haegeman 2015
# 
# This bash script can be used to apply the uparse pipeline to a set of amplicon sequencing samples
# The raw data (F and R reads) of all samples that need to be compared to each other, should be placed in the same folder.
# The folder should also contain a mapping file as needed for qiime.
#Specify here the folder where your samples are:
DIR=/home/genomics/ljoos/RawData_PhD_Bodemmicrobioom/FieldTrials_NGS_Raw_Data/BOPACT/16S

LOCATION=/home/genomics/ljoos/Article_ASV_OTU_comparison/Bopact/Uparse_Pipeline/16S/UPARSE
 
#this is the location of the "rename_seqs_uparse.pl" script:
PERL_SCRIPT_LOCATION=/usr/local/bioinf/uparse_extras

date
echo

#change directory to specified folder
# create several new directories to put the output after each step
mkdir ./1_primers_removed
mkdir ./2_merged
mkdir ./3_filtered_trimming
mkdir ./4_rename_and_concatenate
mkdir ./5_OTU_classification

cd $DIR


# Define the variable FILES that contains all the forward data sets (here only forward, otherwise you will do everything in duplicate!)
# When you make a variable, you can not use spaces! Otherwise you get an error.
FILES=( *_1.fastq.gz )

#Loop over all files and do all the commands
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed (_1.fastq.gz)
SAMPLE=`basename $f _1.fastq.gz`

echo
echo "PROCESSING sample $SAMPLE "
echo

#PRIMER REMOVAL
echo "removing primers for $SAMPLE...."
#remove forward primer using Trimmomatic. 
java -jar /usr/local/bioinf/Trimmomatic-0.32/trimmomatic-0.32.jar SE "$SAMPLE"_1.fastq.gz "$LOCATION"/1_primers_removed/"$SAMPLE"-1.fq HEADCROP:17
#remove reverse primer using Trimmomatic
java -jar /usr/local/bioinf/Trimmomatic-0.32/trimmomatic-0.32.jar SE "$SAMPLE"_2.fastq.gz "$LOCATION"/1_primers_removed/"$SAMPLE"-2.fq HEADCROP:21
echo "done removing primers for $SAMPLE."

# After the primer removal this part can be used for DADA2

#MERGE F AND R READ
echo "merging reads for $SAMPLE...."
usearch -fastq_mergepairs "$LOCATION"/1_primers_removed/"$SAMPLE"-1.fq -reverse "$LOCATION"/1_primers_removed/"$SAMPLE"-2.fq -fastq_truncqual 2 -fastq_maxdiffs 15 -fastq_minovlen 40 -minhsp 10 -fastq_minmergelen 350 -fastqout "$LOCATION"/2_merged/"$SAMPLE"_merged.fastq
echo "done merging reads for $SAMPLE."


#filter reads containing N's (not allowed in DADA2) and with expected errors > 3
#echo "Filtering sequences for $SAMPLE...."
#usearch --fastx_filter "$LOCATION"/2_merged/"$SAMPLE".assembled.fastq --fastqout "$LOCATION"/3_filtered_trimming/"$SAMPLE_merged_filtered.fq --fastq_maxee 3 
#vsearch --fastx_filter "$LOCATION"/2_merged/"$SAMPLE"_merged.fastq --fastqout "$LOCATION"/3_filtered_trimming/"$SAMPLE"_merged_filtered.fastq --fastq_maxee 3 --fastq_maxns 0


#echo "Done filtering sequences for $SAMPLE."
#echo

#QUALITY STATISTICS this part is equal the above part. (lisa)
echo "calculating quality statistics and trimming $SAMPLE...."
usearch -fastq_stats "$LOCATION"/2_merged/"$SAMPLE"_merged.fastq -log "$LOCATION"/3_filtered_trimming/"$SAMPLE"_fastqstats.log
vsearch --fastx_filter "$LOCATION"/2_merged/"$SAMPLE"_merged.fastq --fastaout "$LOCATION"/3_filtered_trimming/"$SAMPLE"_merged_filtered.fa --fastq_maxee 3 --fastq_maxns 0
vsearch --fastx_filter "$LOCATION"/2_merged/"$SAMPLE"_merged.fastq --fastqout "$LOCATION"/3_filtered_trimming/"$SAMPLE"_merged_filtered.fq --fastq_maxee 3 --fastq_maxns 0
echo "done calculating quality statistics and trimming $SAMPLE."

#RENAME SEQUENCES
echo "renaming sequences from $SAMPLE...."
perl $PERL_SCRIPT_LOCATION/rename_seqs_uparse.pl "$LOCATION"/3_filtered_trimming/"$SAMPLE"_merged_filtered.fa $SAMPLE "$LOCATION"/4_rename_and_concatenate/"$SAMPLE"_merged_filtered_renamed.fa
echo "done renaming sequences from $SAMPLE."

echo
echo "DONE PROCESSING SAMPLE $SAMPLE."
echo
echo "################################"

#create a new variable that is a string which contains all sample names ($SAMPLE) with extension _merged_filtered_renamed.fa

ALLSAMPLES+=""$LOCATION"/4_rename_and_concatenate/"$SAMPLE"_merged_filtered_renamed.fa "

done

#from now on, all samples will be concatenated, and can be processed in single commands only (the loop is not necessary anymore).

#CONCATENATE ALL SEQUENCES
echo
echo "concatenating all samples...."
cat $ALLSAMPLES > "$LOCATION"/4_rename_and_concatenate/all_samples.fa
echo "done concatenating all samples."
echo

#DEREPLICATION
echo
echo "dereplicating reads of all samples...."
vsearch --derep_fulllength "$LOCATION"/4_rename_and_concatenate/all_samples.fa --output "$LOCATION"/5_OTU_classification/derep.fa --sizeout
echo "done dereplicating reads of all samples."
echo

#SORTING
echo
echo "sorting reads according to abundance...."
vsearch --sortbysize "$LOCATION"/5_OTU_classification/derep.fa --output "$LOCATION"/5_OTU_classification/sorted.fa --minsize 2
echo "done sorting reads according to abundance."
echo

#OTU CLUSTERING
echo
echo "clustering OTUs...."
usearch -cluster_otus "$LOCATION"/5_OTU_classification/sorted.fa -otus "$LOCATION"/5_OTU_classification/otus.fa
echo "done clustering OTUs."
echo

#CHIMERA REMOVAL
echo
echo "removing chimeras...."
usearch -uchime_ref "$LOCATION"/5_OTU_classification/otus.fa -db /usr/local/bioinf/uparse_extras/rdp_gold.fa -strand plus -nonchimeras "$LOCATION"/5_OTU_classification/otus_wo_chimeras.fa
echo "done removing chimeras."
echo

#OTU LABELING
echo
echo "labeling OTUs...."
python /usr/local/bioinf/uparse_extras/fasta_number.py "$LOCATION"/5_OTU_classification/otus_wo_chimeras.fa OTU_ > "$LOCATION"/5_OTU_classification/final_otus.fa
echo "done labeling OTUs."
echo


#MAP READS BACK TO OTUS AND TRANSFORM OUTPUT TO OTU TABLE
echo
echo "mapping reads against OTUs...."
vsearch -usearch_global "$LOCATION"/4_rename_and_concatenate/all_samples.fa -db "$LOCATION"/5_OTU_classification/final_otus.fa -strand plus -id 0.97 -uc "$LOCATION"/5_OTU_classification/map.uc
python /usr/local/bioinf/uparse_extras/uc2otutab.py "$LOCATION"/5_OTU_classification/map.uc > "$LOCATION"/5_OTU_classification/OTU_table.txt
biom convert -i "$LOCATION"/5_OTU_classification/OTU_table.txt -o "$LOCATION"/5_OTU_classification/OTU_table.biom --table-type "OTU table" --to-hdf5
echo "done mapping reads against OTUs."
echo

#ASSIGN A TAXONOMY
#This will be done using DADA2

date
