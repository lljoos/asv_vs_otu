#!/bin/bash
# (c) Caroline De Tender 2019
# caroline.detender@ilvo.vlaanderen.be
# This bash script can be used to simulate amplicon datasets making use of the program grinder
# You can simulate your data from all different kind of input datasets. Here we will make use of the Silva database v132 (Bacteria) and the UNITE database (Fungi)

#Specify here the folder where your samples are:
INPUT_FOLDER=/home/genomics/cdetender/3_FWO_postdoc/WP3/1_ASV_OTU_comparison/Synthetic_mock_communities
#Specify here the folder where you want your output files to be written to:
OUTPUT_FOLDER=/home/genomics/cdetender/3_FWO_postdoc/WP3/1_ASV_OTU_comparison/Synthetic_mock_communities/Diversity_2500_Final

date
echo

#change directory to specified folder
cd $INPUT_FOLDER

# create several new directories to put the output after each step
mkdir ./Diversity_2500_Final

#Define the variable input datasets
input = usr/share/qiime/data/silva_core_sets/Silva132_release/SILVA_132_QIIME_release/rep_set/rep_set_all/97/silva132_97.fna
#input = /usr/share/bioinf_databases/UNITE/v020219/Fungi/developer/sh_general_release_dynamic_02.02.2019_dev.fasta

#Define the file containing the primers to be used to construct the amplicon library from
primers = ./16S_V3V4_primers.fna  #V3V4 16S rRNA gene primers
primers = ./ITS2_primers.fna #ITS2 fungal gene primers

####construct a synthetic community (first line = PE, second line = SE)
grinder -rf input -tr 100000 -rd 300 -id 464 -mo FR -fr primers -un 1 -cb 1 -di 2500 -ql 30 30 -od OUTPUT_FOLDER -nl 3 -sp 90 -am powerlaw 1 -fq 1 -md poly4 3e-3 3.3e-8 #PE reads_1
grinder -rf input -tr 100000 -rd 464 -fr primers -un 1 -cb 1 -di 2500 -ql 30 30 -od OUTPUT_FOLDER -nl 3 -sp 90 -am powerlaw 1 -fq 1 -md poly4 3e-4 3.3e-10 #SE reads_1

#####Transformation of the dataset of we are working with PE files
#Split dataset in forward and reverse strands for PE reads
grep "@*/1" grinder_1_reads.fq > grinder_1_reads_1.fastq   #grep forward files
grep "@*/2" grinder_1_reads.fq > grinder_1_reads_2.fastq   #grep reverse files

#rename sequence file for PE and SE reads
cp grinder_1_reads_1.fastq ./Final_communities/Div2500-1-reads_1.fastq
cp grinder_1_reads_2.fastq ./Final_communities/Div2500-1-reads_2.fastq

#remove from those sequences the -- from PE files
sed -i '/--/d' ./Div2500-1-reads_1.fastq
sed -i '/--/d' ./Div2500-1-reads_2.fastq

#For merging the samples, the headers need to be identical. Therefore, additional information, e.g. errors should be removed. 
awk -F " " '/^@/ { print $1,$2 ; next} 1' Div2500-1-reads_1.fastq  #take all sequences starting with @, separate on spaces, select first two parts with spaces and retain other lines as well
awk -F " " '/^@/ { print $1,$2 ; next} 1' Div2500-1-reads_2.fastq 

sed -i 's#/2#/1#g' Div2500-1-reads_header_2.fastq  #change the first part of the reverse read header from @1_1/2 to @1_1/1


###Retrieving taxonomic information out of the dataset.
#FIRST we are going to select the sequence ids out of the grinder-read files (so linked with the number of the sequence). These contain the information linked to the taxon. 
awk -F " " '/^@/ { print $1,$2; next} 1' D2500-1.fastq | grep "@1*" | sed 's/reference=//' > ./7_Real_Taxonomy_Comparison/Real_TaxonID_D2500-1.txt

#To add the specific taxonomy per sequence ID per sample, following code can be used
awk 'NR==FNR{a[$1]=$2 ; next} $2 in a {$2=$2 FS a[$2]} 1' /usr/share/qiime/data/silva_core_sets/Silva132_release/SILVA_132_QIIME_release/taxonomy/taxonomy_all/97/consensus_taxonomy_7_levels.txt ./7_Real_Taxonomy_Comparison/Real_TaxonID_D2500-1

#SECOND, we will look to the OTU table linked with the sequence header, to link the sequenceID with the taxonomy later on.
#output e.g. 1_2;barcodelabel=D1000-1	OTU_286
cut -f 9,10 ./4_OTU_classification/map.uc > ./7_Real_Taxonomy_Comparison/OTU_Sequence_Link_Uparse.txt

#To this output, we will add the taxonomy that was assigned to QIIME for the specific OTU
#output e.g. 1_2;barcodelabel=D1000-1	OTU_286	D_0__Bacteria;D_1...
awk 'NR==FNR{a[$1]=$2 ; next} $2 in a {$2=$2 FS a[$2]} 1' ./5_taxonomy_assignment/otuse_assigned/final_otus_tax_assignments.txt ./7_Real_Taxonomy_Comparison/OTU_Sequence_Link_Uparse.txt > ./7_Real_Taxonomy_Comparison/OTU_Sequence_Link_Uparse_with_Taxonomy.txt

#The rest of the workflow to compare these samples with each other has been done in Rstudio. 


#To retrieve the information out of the grinder-rank files:
#cut -f 2 ./Div2500-1-ranks.txt | awk -F. '{print $1}' > accession_number_Div2500-1.txt
#cut -f 1,2 ./Div2500-1-ranks.txt  > accession_number_Div2500-1.txt
	
	#This will first select the second column out of the rank files	
	#Thereafter, we are going to select merely the accession number, which is the value before .1.
	#These accession numbers are stored in a new txt file
	#with this value we will work further to look up the accesion number
	
	https://github.com/sherrillmix/taxonomizr
